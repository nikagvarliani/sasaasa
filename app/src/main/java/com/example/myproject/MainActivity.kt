package com.example.myproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val bottomnavview = findViewById<BottomNavigationView>(R.id.navigacia)
        val bolosafrag = findNavController(R.id.fragmenti_ukve_mzad)

        val seti = setOf<Int>(
            R.id.firstfragment,
            R.id.fourthfragment,
            R.id.secondfragment,
            R.id.thirdfragment,

        )
        setupActionBarWithNavController(bolosafrag, AppBarConfiguration(seti))
        bottomnavview.setupWithNavController(bolosafrag)
    }
}