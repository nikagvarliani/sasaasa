package com.example.myproject.fragment

import android.os.Bundle
import android.text.Layout
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.myproject.R

class firstfragment : Fragment(R.layout.fragment_first) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sendgilaki = view.findViewById<Button>(R.id.button)
        val edittext = view.findViewById<EditText>(R.id.editText)
        val controller = Navigation.findNavController(view)

        sendgilaki.setOnClickListener {
            val nameText = edittext.text.toString()
            if (nameText.isEmpty()) {
                return@setOnClickListener
            }
            val name = nameText.toInt()
            val action = firstfragmentDirections.actionFirstfragmentToSecondfragment(name)

            controller.navigate(action)


        }
    }


}





